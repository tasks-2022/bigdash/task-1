import { generateTeams } from "./team";

export function main() {
  const { team1, team2 } = generateTeams();

  console.log("First Team: ");
  team1.players.forEach((element) => {
    console.log(element.name + " " + element.rate);
  });
  console.log(`The sum of first team is : ${team1?.totalRate}`);

  console.log("Second Team: ");
  team2.players.forEach((element) => {
    console.log(element.name + " " + element.rate);
  });
  console.log(`The sum of second team is : ${team2?.totalRate}`);
}

main();
