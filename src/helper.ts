// Function to generate a random rating between 1 and 100
export function generateRandomRating() {
  return Math.floor(Math.random() * 10) + 1;
}

// Shuffle the array
export function shuffleArray(arr: number[]) {
  for (let i = arr.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }
}

// Get random name
export function getRandomName() {
  const names = [
    "Mohammed",
    "Ahmed",
    "Ali",
    "Hassan",
    "Abdullah",
    "Omar",
    "Ibrahim",
    "Yusuf",
    "Aayan",
    "Abbas",
    "Abdul",
    "Adnan",
    "Ahmad",
    "Akeem",
    "Hassan",
    "Jamal",
    "Kareem",
    "Omar",
    "Salman",
  ];
  let firstNameIndex = Math.floor(Math.random() * names.length);
  let lastNameIndex = Math.floor(Math.random() * names.length);
  return `${names[firstNameIndex]} ${names[lastNameIndex]}`;
}
