"use strict";
exports.__esModule = true;
exports.main = void 0;
var team_1 = require("./team");
function main() {
    var _a = (0, team_1.generateTeams)(), team1 = _a.team1, team2 = _a.team2;
    console.log("First Team: ");
    team1.players.forEach(function (element) {
        console.log(element.name + " " + element.rate);
    });
    console.log("The sum of first team is : ".concat(team1 === null || team1 === void 0 ? void 0 : team1.totalRate));
    console.log("Second Team: ");
    team2.players.forEach(function (element) {
        console.log(element.name + " " + element.rate);
    });
    console.log("The sum of second team is : ".concat(team2 === null || team2 === void 0 ? void 0 : team2.totalRate));
}
exports.main = main;
main();
