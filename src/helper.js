"use strict";
exports.__esModule = true;
exports.getRandomName = exports.shuffleArray = exports.generateRandomRating = void 0;
// Function to generate a random rating between 1 and 100
function generateRandomRating() {
    return Math.floor(Math.random() * 10) + 1;
}
exports.generateRandomRating = generateRandomRating;
// Shuffle the array
function shuffleArray(arr) {
    for (var i = arr.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
exports.shuffleArray = shuffleArray;
// Get random name
function getRandomName() {
    var names = [
        "Mohammed",
        "Ahmed",
        "Ali",
        "Hassan",
        "Abdullah",
        "Omar",
        "Ibrahim",
        "Yusuf",
        "Aayan",
        "Abbas",
        "Abdul",
        "Adnan",
        "Ahmad",
        "Akeem",
        "Hassan",
        "Jamal",
        "Kareem",
        "Omar",
        "Salman",
    ];
    var firstNameIndex = Math.floor(Math.random() * names.length);
    var lastNameIndex = Math.floor(Math.random() * names.length);
    return "".concat(names[firstNameIndex], " ").concat(names[lastNameIndex]);
}
exports.getRandomName = getRandomName;
