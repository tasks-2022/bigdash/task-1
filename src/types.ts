export type Player = {
  id: number;
  name: string;
  rate: number;
};

export type Team = {
  players: Player[];
  totalRate: number;
};
