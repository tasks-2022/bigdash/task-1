import { getRandomName, shuffleArray } from "./helper";
import { Team } from "./types";

export function createTeam(ratingList: number[]) {
  let team: Team = { players: [], totalRate: 0 };
  for (let index = 0; index < ratingList.length; index++) {
    team.players.push({
      id: index,
      name: getRandomName(),
      rate: ratingList[index],
    });
    team.totalRate = team.totalRate + ratingList[index];
  }
  return team;
}

export function generateTeams() {
  // Create an array of 10 numbers from 1 to 10
  // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  let ratingList = Array.from({ length: 10 }, (_, i) => i + 1);

  // for example [1, 6, 5, 2, 3, 7, 9, 4, 8, 10]
  shuffleArray(ratingList);

  // Remove the last 3 items
  // [1, 6, 5, 2, 3, 7, 9]
  ratingList.length = ratingList.length - 3;

  let team1: Team = { players: [], totalRate: 0 };
  let team2: Team = { players: [], totalRate: 0 };
  team1 = createTeam(ratingList);
  shuffleArray(ratingList);
  team2 = createTeam(ratingList);

  return {
    team1: team1,
    team2: team2,
  };
}
