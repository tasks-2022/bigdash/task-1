"use strict";
exports.__esModule = true;
exports.generateTeams = exports.createTeam = void 0;
var helper_1 = require("./helper");
function createTeam(ratingList) {
    var team = { players: [], totalRate: 0 };
    for (var index = 0; index < ratingList.length; index++) {
        team.players.push({
            id: index,
            name: (0, helper_1.getRandomName)(),
            rate: ratingList[index]
        });
        team.totalRate = team.totalRate + ratingList[index];
    }
    return team;
}
exports.createTeam = createTeam;
function generateTeams() {
    // Create an array of 10 numbers from 1 to 10
    var ratingList = Array.from({ length: 10 }, function (_, i) { return i + 1; });
    (0, helper_1.shuffleArray)(ratingList);
    // Remove the last 3 items
    ratingList.length = ratingList.length - 3;
    var team1 = { players: [], totalRate: 0 };
    var team2 = { players: [], totalRate: 0 };
    team1 = createTeam(ratingList);
    (0, helper_1.shuffleArray)(ratingList);
    team2 = createTeam(ratingList);
    return {
        team1: team1,
        team2: team2
    };
}
exports.generateTeams = generateTeams;
